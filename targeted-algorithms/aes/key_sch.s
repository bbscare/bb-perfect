/*
 * File: key_sch.s
 * Project: aes
 * Created Date: Thursday April 22nd 2021
 * Author: Ronan (ronan.lashermes@inria.fr)
 * -----
 * Last Modified: Thursday, 10th June 2021 4:12:36 pm
 * Modified By: Ronan (ronan.lashermes@inria.fr>)
 * -----
 * Copyright (c) 2021 INRIA
 */

#[global]
@next_round_key://a0 contains the round
    push ra;
    a1 <- a0;
    a0 <-t0 [0x101C];
    push a0;//save last previous column

    call &key_col_transform;//do the complex transformation

    t1 <-t0 [0x1010];
    t1 <- t1 ^ a0;
    [0x1010] <-t0 t1;

    t0 <-t0 [0x1014];
    t1 <- t1 ^ t0;
    [0x1014] <-t0 t1;

    t0 <-t0 [0x1018];
    t1 <- t1 ^ t0;
    [0x1018] <-t0 t1;

    pop t0;//previous value at [0x101C]
    t1 <- t1 ^ t0;
    [0x101C] <-t0 t1;

    pop ra;
    return;

@key_col_transform://expect input column in a0, the round number in a1
    push ra;
    push a1;

    // rotword
    t0 <- a0 << 8;
    t1 <- a0 >> 24;
    a0 <- t0 | t1;

    // subword
    call &subpart;

    // rcon
    pop a1;
    t1 <- &rcon;
    t1 <- t1 + a1;
    t1 <- [t1]b;//load rcon value according to round number

    t0 <- a0 >> 24;
    t0 <- t0 ^ t1;

    a0 <- a0 << 8;
    a0 <- a0 >> 8;
    t0 <- t0 << 24;
    a0 <- a0 ^ t0;

    pop ra;
    return;

def rcon: [u8] = [0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40,	0x80, 0x1B, 0x36];