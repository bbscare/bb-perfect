# Black-Box SCARE

Proof-of-concept for the perfect scenario.

**Require 32 GB of RAM and 130 GB of disk space.**
Computation take several hours, depending on computing power.

## Install Nix (to do once if not already present on the test machine)

We use [Nix](https://nixos.org/download.html) for a reproducible environnement.
To install Nix (multi-user config) on your machine:

```bash
sh <(curl -L https://nixos.org/nix/install) --daemon
```

The scripts will use the Nix configuration [env/shell.nix](./env/shell.nix).
The software used (rustc and julia notably) won't interfere with existing versions on your machine.

## Launching the nix shell

All commands must be entered into the nix shell, launched from the git root folder with
```bash
nix-shell env/shell.nix
```

(Do not add the *--pure** argument at first, since we will need to fetch packages over the internet.)

## Generate trace data

To generate trace data (6M traces),

```bash
rvem -i $AES -o $DATA/io -t $DATA/traces_raw.binu8 trace -c 6000000
```

Replace $AES with $TWINE to target this algo instead.

## Perform the analysis

The analysis is performed by the rust application [perfect-analysis](./apps/perfect-analysis/).
To compile and execute :

```
cd apps/perfect-analysis
cargo run --release
```

The computation will last a few hours, depending on your computer power.
Should require at least 32GB of RAM, the more CPU cores, the better.

At the end of the computation, we use the predictor to verify a hardcoded test vector.
A correct result should end with :

```
PT: 00112233445566778899aabbccddeeff
K: 000102030405060708090a0b0c0d0e0f
Predicted CT: 69c4e0d86a7b0430d8cdb78070b4c55a
True CT: 69c4e0d86a7b0430d8cdb78070b4c55a
SUCCESS: true
```