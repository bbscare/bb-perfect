use std::path::Path;

use serde::{Serialize, de::DeserializeOwned};

pub trait FileStorable: Serialize + DeserializeOwned + Sized {
    fn from_file<P: AsRef<Path>>(path: P) -> anyhow::Result<Self> {
        let bytes: Vec<u8> = std::fs::read(path.as_ref())?;
        let d: Self = rmp_serde::from_slice::<Self>(&bytes)?;
        return Ok(d);
    }

    fn save_to_file<P: AsRef<Path>>(&self, path: P) -> anyhow::Result<()> {
        let bytes = rmp_serde::to_vec(&self)?;
        std::fs::write(path, &bytes)?;
        return Ok(());
    }
}