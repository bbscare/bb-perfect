use std::{collections::{HashMap, HashSet}, cmp::{min, Ordering}, ops::Range};
use std::fmt;

// use hashbrown::HashSet;
use rayon::prelude::IntoParallelIterator;
use serde::{Serialize, Deserialize};

use crate::loaders::{trace_matrix::TraceMatrixColumns, file_storable::FileStorable};
use crate::information::flow_analysis::TwoSourcesAnalyzer;
use crate::information::entropies::EntropyEstimator;

use super::{flow_analysis::FaiMatrix, clusters::Clusters};
use rayon::iter::ParallelIterator;
use indicatif::{ParallelProgressIterator, ProgressIterator};

#[derive(Clone,Debug,Serialize,Deserialize,PartialEq,Eq,Hash,Copy)]
pub enum HEdgeInput {
    One(usize),
    Two(usize,usize) // must enforce .1 < .2
}

impl HEdgeInput {
    pub fn new2(i1: usize, i2: usize) -> HEdgeInput {
        if i1 == i2 {
            HEdgeInput::One(i1)
        }
        else if i1 < i2 {
            HEdgeInput::Two(i1,i2)
        }
        else {
            HEdgeInput::Two(i2, i1)
        }
    }

    pub fn new1(i1: usize) -> HEdgeInput {
        HEdgeInput::One(i1)
    }

    pub fn raw_to_rich(&self, clusters: &Clusters) -> HEdgeInput {
        match self {
            HEdgeInput::One(i1) => HEdgeInput::new1(clusters.map_raw_to_logic(*i1)),
            HEdgeInput::Two(i1, i2) => {
                let logic_i1: usize = clusters.map_raw_to_logic(*i1);
                let logic_i2: usize = clusters.map_raw_to_logic(*i2);
                if logic_i1 == logic_i2 {
                    HEdgeInput::new1(logic_i1)
                }
                else {
                    HEdgeInput::new2(logic_i1, logic_i2)
                }
            }
        }
    }
}

#[derive(Clone,Debug,Serialize,Deserialize,PartialEq,Eq,Hash,Copy)]
pub struct HEdge {
    pub inputs: HEdgeInput,
    pub output: usize,
}

impl HEdge {
    pub fn new2to1(i1: usize, i2: usize, i3: usize) -> HEdge {
        assert!(i1 < i3 && i2 < i3);
        HEdge { inputs: HEdgeInput::new2(i1,i2), output: i3 }
    }

    pub fn new1to1(i1: usize, i2: usize) -> HEdge {
        assert!(i1 < i2);
        HEdge { inputs: HEdgeInput::new1(i1), output: i2 }
    }

    pub fn is1to1(&self) -> bool {
        match self.inputs {
            HEdgeInput::One(_) => true,
            _ => false
        }
    }

    pub fn is2to1(&self) -> bool {
        match self.inputs {
            HEdgeInput::Two(_,_) => true,
            _ => false
        }
    }
}

impl PartialOrd for HEdgeInput {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match self {
            HEdgeInput::One(s_i1) => {
                match other {
                    HEdgeInput::One(o_i1) => Some(s_i1.cmp(o_i1)),
                    HEdgeInput::Two(_, _) => Some(Ordering::Less)
                }
            },
            HEdgeInput::Two(s_i1, s_i2) => {
                match other {
                    HEdgeInput::One(_) => Some(Ordering::Greater),
                    HEdgeInput::Two(o_i1, o_i2) => {
                        if s_i1 == o_i1 && s_i2 == o_i2 {
                            Some(Ordering::Equal)
                        }
                        else if s_i1 <= o_i1 && s_i2 <= o_i2 {
                            Some(Ordering::Less)
                        }
                        else if s_i1 >= o_i1 && s_i2 >= o_i2 {
                            Some(Ordering::Greater)
                        }
                        else {
                            None
                        }
                    }
                }
            }
        }
    }
}

impl fmt::Display for HEdge {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        // write!(f, "({}, {})", self.x, self.y)
        match self.inputs {
            HEdgeInput::One(i1) => {
                write!(f, "{{{}}} -> {}", i1, self.output)
            },
            HEdgeInput::Two(i1, i2) => {
                write!(f,"{{{}, {}}} -> {}", i1, i2, self.output)
            }
        }
    }
}

#[derive(Clone,Debug,Serialize,Deserialize)]
pub struct HGraph {
    pub edges: HashMap<HEdge, f32>,
}

impl HGraph {
    pub fn new() -> HGraph {
        HGraph { edges: HashMap::new() }
    }

    pub fn remove_edge(&mut self, edge: &HEdge) {
        self.edges.remove(&edge);
    }

    pub fn add_edge(&mut self, edge: HEdge, weight: f32) {
        self.edges.insert(edge, weight);
    }

    pub fn from_fai_matrix(fai_mat: &FaiMatrix, noise_threshold: f32) -> HGraph {
        // let len = fai_mat.len();
        let mut hg = HGraph::new();
        let threshold = 1f32 - noise_threshold;

        for ((i1, i2), w) in fai_mat.values.iter() {
            if *w < threshold {
                hg.add_edge(HEdge::new1to1(*i1, *i2), *w);
            }
        }

        hg
    }

    pub fn range_outputs(&self) -> Range<usize> {
        let mut min_output = usize::MAX;
        let mut max_output: usize = usize::MIN;

        for (e, _) in self.edges.iter() {
            let o = e.output;
            if o < min_output {
                min_output = o;
            }
            if o > max_output {
                max_output = o;
            }
        } 

        return min_output..(max_output+1);
    }

    pub fn ensure_timing_causality(&mut self) {
        self.edges.retain(|e, _| {
            let max_input = match e.inputs {
                HEdgeInput::One(i) => i,
                HEdgeInput::Two(i1, i2) => {
                    if i1 > i2 {
                        i1
                    }
                    else {
                        i2
                    }
                }
            };
            max_input < e.output
        });
    }

    pub fn progressive_simplification(&mut self, noise_threshold: f32) {

        let output_range = self.range_outputs();

        // 1. keep minimum by weight +- noise
        let output_range_len = output_range.len() as u64;
        for i3 in output_range.progress_count(output_range_len) {
            let edges = self.edges_ending_in(i3);

            let mut minus = f32::MAX;
            // compute minus
            for (_, w) in edges.iter() {
                if *w < minus {
                    minus = *w;
                }
            }

            

            // remove all bigger than minus + noise
            let minus_with_noise = minus + noise_threshold;
            for (e, w) in edges.iter() {
                if *w > minus_with_noise {
                    self.remove_edge(e);
                }
            }
        }
    }

    const VEC_SIZE_CAP: usize = 3;
    pub fn minimum_ancestor_sets(&self, i3s: Vec<usize>) -> Vec<HEdgeInput> {
        let mut ancestor_sets: Vec<HEdgeInput> = Vec::new();

        let mut to_explore: Vec<Vec<usize>> = Vec::new();
        to_explore.push(i3s.clone());

        let mut explored: HashSet<Vec<usize>> = HashSet::new();

        while !to_explore.is_empty() {
            let current_set = to_explore.pop().unwrap(); // can unwrap because of loop test
            explored.insert(current_set.clone());

            for (t_index, t) in current_set.iter().enumerate() {
                let mut current_set_minus_t = current_set.clone();
                current_set_minus_t.remove(t_index);

                let edges_ending_in_t = self.edges_ending_in(*t);
                for (incoming_edge, _) in edges_ending_in_t.iter() {
                    // substitute t by incoming edge sources
                    // println!("{:?} + {:?}", current_set_minus_t, incoming_edge.inputs);
                    let mut new_set = current_set_minus_t.clone();
                    match incoming_edge.inputs {
                        HEdgeInput::One(i1) => {
                            new_set.push(i1);
                            new_set.sort();
                        },
                        HEdgeInput::Two(i1,i2) => {
                            new_set.push(i1);
                            new_set.push(i2);
                            new_set.sort();
                        }
                    }

                    if new_set.len() <= HGraph::VEC_SIZE_CAP {
                        if !explored.contains(&new_set) && !to_explore.contains(&new_set) {
                            to_explore.push(new_set);
                        }
                    }
                }
            }
        }

        explored.remove(&i3s);

        for mut s in explored.into_iter() {
            let input_opt = match s.len() {
                1 => Some(HEdgeInput::new1(s.pop().unwrap())),
                2 => Some(HEdgeInput::new2(s.pop().unwrap(), s.pop().unwrap())),
                _ => None
            };

            if let Some(input) = input_opt {
                // ancestor_sets.push(input);
                let mut to_insert = true;
                let mut to_remove: Option<Vec<HEdgeInput>> = None;
                for other_ancestors in ancestor_sets.iter() {
                    
                    // println!("{:?} ? {:?} : {:?}", input, other_ancestors, input.partial_cmp(other_ancestors));
                    match input.partial_cmp(other_ancestors) {
                        Some(Ordering::Less) => {
                            // remove other

                            let new_to_remove = match to_remove {
                                None => {Some(vec![other_ancestors.clone()])},
                                Some(mut edges) => { 
                                    edges.push(other_ancestors.clone());
                                    Some(edges)
                                }
                            };
                            to_remove = new_to_remove;

                            to_insert = true;
                        },
                        Some(Ordering::Greater) => {
                            // do not push
                            to_insert = false;
                        },
                        Some(Ordering::Equal) => {
                            to_insert = false;
                        },
                        _ => {
                            to_insert = true;
                        }
                    }
                }

                if let Some(edges_to_remove) = to_remove {
                    ancestor_sets.retain(|e| !edges_to_remove.contains(e));
                }
                if to_insert == true {
                    ancestor_sets.push(input);
                }
            }
        }

        return ancestor_sets;
    }


    pub fn edges_ending_in(&self, t: usize) -> HashMap<HEdge, f32> {
        self.edges.iter().filter(|(e, _)| {
            e.output == t
        }).map(|(pe, pw)| (*pe, *pw)).collect()
    }

    pub fn edges_starting(&self, t: usize) -> HashMap<HEdge, f32> {
        self.edges.iter().filter(|(e, _)|  match e.inputs {
            HEdgeInput::One(i1) => i1 == t,
            HEdgeInput::Two(i1, i2) => i1 == t || i2 == t,
        }).map(|(pe, pw)| (*pe, *pw)).collect()
    }

    pub fn search_for_2to1(&mut self, traces: &TraceMatrixColumns, estimator: &EntropyEstimator, noise_threshold: f32, window: usize) -> anyhow::Result<()>{
        
        let traces_len = traces.traces_len();
        let traces_count = traces.traces_count();

        assert!(traces_len > 0 && traces_count > 0);

        let i1range: Vec<usize> = (0..(traces_len-2)).collect();
        let i1_len: u64 = i1range.len() as u64;

        let resulting_edges = i1range.into_par_iter().progress_count(i1_len).map(|i1| {
            let i2max = min(traces_len-1, i1 + window);
            let mut edges: HashMap<HEdge, f32> = HashMap::new();
            for i2 in (i1+1)..i2max {
                let analyzer = TwoSourcesAnalyzer::new(traces, i1, i2, &self, estimator, noise_threshold, window);
                for (i3, w) in analyzer.identified_2to1.iter() {
                    edges.insert(HEdge::new2to1(i1, i2, *i3), *w);
                }
            }
            return edges;
        });

        let new_edges: HashMap<HEdge, f32> = resulting_edges.reduce(|| {let h: HashMap<HEdge, f32> = HashMap::new(); h}, |mut a, b| {a.extend(b); a});
        self.edges.extend(new_edges);

        Ok(())
    }

    pub fn edges_count(&self) -> usize {
        self.edges.len()
    }

    pub fn edge_types_count(&self) -> (usize, usize) { //1to1, 2to1
        let mut c1to1 = 0;
        let mut c2to1 = 0;

        for (e, _) in self.edges.iter() {
            match e.inputs {
                HEdgeInput::One(_) => {c1to1 += 1;},
                HEdgeInput::Two(_, _) => {c2to1 += 1;}
            }
        }

        (c1to1, c2to1)
    }

    pub fn edge_weight(&self, edge: &HEdge) -> f32 {
        return *self.edges.get(edge).unwrap_or(&1f32)
    }

    pub fn pretty_print_edges(edges: &HashMap<HEdge, f32>) {
        for (e, w) in edges.iter() {
            println!("\t{} ({})", e, w);
        }
    }

    pub fn to_logic_hgraph(&self, clusters: &Clusters) -> HGraph {
        let mut logic_hg = HGraph::new();

        //replace all edges with edges from rich nodes

        let edge_count = self.edges.len() as u64;
        for (edge, weight) in self.edges.iter().progress_count(edge_count) {
            let raw_inputs = edge.inputs;
            let raw_output = edge.output;

            let logic_output: usize = clusters.map_raw_to_logic(raw_output);
            let logic_inputs = raw_inputs.raw_to_rich(clusters);

            let self_edge = match logic_inputs {
                HEdgeInput::One(i1) => {
                    i1 == logic_output
                },
                HEdgeInput::Two(i1, i2) => {
                    i1 == logic_output || i2 == logic_output
                }
            };

            if !self_edge {
                logic_hg.add_edge(HEdge { inputs: logic_inputs, output: logic_output}, *weight);
            }
        }

        logic_hg
    }

    pub fn update_weight(&mut self, traces: &TraceMatrixColumns, estimator: &EntropyEstimator) {
        let _ = self.edges.iter_mut().map(|(e,w)| {
            let new_weight = traces.fai_hedge(e, estimator).unwrap();
            if (*w-new_weight).abs() < 0.001f32 {
                println!("Updating {} : {} becomes {}", e, w, new_weight)
            }
            *w = new_weight;
            // (e,w)
        });
    }
}

impl FileStorable for HGraph {}

#[cfg(test)]
mod tests {
    use crate::{loaders::trace_matrix::TraceMatrixColumns, information::entropies::EntropyEstimator};
    use crate::loaders::file_storable::FileStorable;
    use super::HGraph;


    #[test]
    fn search_2to1() {
        let mut hg = HGraph::from_file("../data/hg").unwrap();
        // let init_nodes: Vec<usize> = (0..16).collect();
        let traces = TraceMatrixColumns::from_file("../data/poor_traces_if.binu8").unwrap();
        let estimator = EntropyEstimator::from_file("../data/entropy_estimators").unwrap();

        hg.search_for_2to1(&traces, &estimator, 0.1, 256).unwrap();
    }
}