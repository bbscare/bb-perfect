use std::collections::HashMap;

use indicatif::ProgressBar;
use serde::{Serialize, Deserialize};

use crate::loaders::{trace_matrix::TraceMatrixLinesJointBatchReader, file_storable::FileStorable};


#[derive(Debug,Clone, Serialize, Deserialize)]
pub struct IOFunctions {
    pub input_funcs: Vec<HashMap<u8, u8>>, // (input time -> (1to1function))
    pub inputs_mapping: Vec<usize>, // (input time -> corresponding time in traces)

    pub output_funcs: Vec<HashMap<u8, u8>>, // (output time -> (1to1function))
    pub outputs_mapping: Vec<usize>, // (output time -> corresponding time in traces)
}

impl IOFunctions {
    pub fn learn(traces_loader: &mut TraceMatrixLinesJointBatchReader, inputs_mapping: &Vec<usize>, outputs_mapping: &Vec<usize>) -> anyhow::Result<IOFunctions> {
        
        
        let mut input_funcs: Vec<HashMap<u8, u8>> = Vec::new();
        let mut output_funcs: Vec<HashMap<u8, u8>> = Vec::new();

        // prefill funcs with empty functions for now
        for _ in inputs_mapping.iter() {
            input_funcs.push(HashMap::new());
        }

        for _ in outputs_mapping.iter() {
            output_funcs.push(HashMap::new());
        }



        let steps_count = traces_loader.steps();
        let bar = ProgressBar::new(steps_count as u64);
        while !traces_loader.is_terminated() {
            let (inputs, traces, outputs) = traces_loader.read_batches()?;

            let batch_len = traces.len();
            for trace_index in 0..batch_len {
                // find inputs
                for (i, input_time) in inputs_mapping.iter().enumerate() { // (input index, input time in trace)
                    match input_funcs[i].get(&inputs[trace_index][i]) {
                        Some(v3) => { //check
                            if *v3 != traces[trace_index][*input_time] {
                                return Err(anyhow::format_err!("Error in the learning phase for input byte {}", i));
                            }
                        },
                        None => { // create entry
                            input_funcs[i].insert(inputs[trace_index][i], traces[trace_index][*input_time]);
                        }
                    }
                }


                //find outputs
                for (i, output_time) in outputs_mapping.iter().enumerate() { // (input index, input time in trace)
                    match output_funcs[i].get(&traces[trace_index][*output_time]) {
                        Some(v3) => { //check
                            if *v3 != outputs[trace_index][i] {
                                return Err(anyhow::format_err!("Error in the learning phase for output byte {}", i));
                            }
                        },
                        None => { // create entry
                            output_funcs[i].insert(traces[trace_index][*output_time], outputs[trace_index][i]);
                        }
                    }
                }
            }
            
            bar.inc(1);
        }
        bar.finish();


        Ok(IOFunctions { input_funcs, inputs_mapping: inputs_mapping.clone(), output_funcs, outputs_mapping: outputs_mapping.clone() })
    }

    pub fn apply_input(&self, input_index: usize, input_value: u8) -> anyhow::Result<u8> {
        match self.input_funcs[input_index].get(&input_value) {
            Some(v) => Ok(*v),
            None => Err(anyhow::format_err!("Input value {} unknow for byte {}", input_value, input_index))
        }
    }

    pub fn apply_output(&self, output_index: usize, trace_value: u8) -> anyhow::Result<u8> {
        match self.output_funcs[output_index].get(&trace_value) {
            Some(v) => Ok(*v),
            None => Err(anyhow::format_err!("Trace value {} unknow for byte {}", trace_value, output_index))
        }
    }
}

impl FileStorable for IOFunctions {}