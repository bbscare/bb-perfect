use std::{collections::HashMap, path::Path};

use indicatif::{ParallelProgressIterator, ProgressBar};
use itertools::min;
use rayon::prelude::IntoParallelIterator;
use rayon::iter::ParallelIterator;

use serde::{Serialize, Deserialize};

use crate::loaders::{file_storable::FileStorable, trace_matrix::{TraceMatrixLinesJointBatchReader, TraceMatrixLinesBatchWriter, SimpleFilteringResults}};

use super::hgraph::{HEdgeInput, HGraph};


#[derive(Debug,Clone,Serialize,Deserialize)]
pub struct Clusters {
    clusters: HashMap<HEdgeInput, Vec<usize>>,
    mappings_to_head: HashMap<usize, usize>, // element, cluster head
    raw_to_logic_mappings: HashMap<usize, usize>, //raw time, logic time
    raw_time_to_cluster_key: HashMap<usize, HEdgeInput>, //raw time, cluster key
    positions: HashMap<HEdgeInput, usize>, // cluster key, position in logic trace
    inv_positions: HashMap<usize, HEdgeInput>, // position in logic trace, cluster key
    clustering_functions: HashMap<HEdgeInput, HashMap<Vec<u8>, u8>>, // (cluster key, func = Vec<u8> -> u8)
}

impl FileStorable for Clusters {}

impl Clusters {
    pub fn from_hgraph(hg: &HGraph) -> Clusters {
        let mut mappings_to_head: HashMap<usize, usize> = HashMap::new();
        let mut raw_to_logic_mappings: HashMap<usize, usize> = HashMap::new();
        let mut clusters: HashMap<HEdgeInput, Vec<usize>> = HashMap::new();

        let output_range = hg.range_outputs();
        let output_range_len: u64 = output_range.len() as u64;

        let cluster_mappings: Vec<(Vec<HEdgeInput>, usize)> = output_range.into_par_iter().progress_count(output_range_len).map(|i3| {
            (hg.minimum_ancestor_sets(vec![i3]), i3)
        }).collect();


        // merge clusters with same source edge
        for (c_heads, i3) in cluster_mappings.into_iter() {

            assert!(c_heads.len() == 1);
            let c_head = c_heads[0];
            match clusters.get_mut(&c_head) {
                Some(old) => {old.push(i3)},
                None => { clusters.insert(c_head.clone(), vec![i3]);}
            }
        }

        // add one to one sources in clusters
        for (head_edge, cluster) in clusters.iter_mut() {
            match head_edge {
                HEdgeInput::One(i1) => { cluster.insert(0,*i1); },
                _ => {}
            }
        }

        // compute mappings
        let mut raw_time_to_cluster_key: HashMap<usize, HEdgeInput> = HashMap::new();
        for (chead, cluster) in clusters.iter() {
            let head_time = cluster.first().unwrap(); //vec must be sorted
            for t in cluster.iter() {
                mappings_to_head.insert(*t, *head_time);
                raw_time_to_cluster_key.insert(*t, *chead);
            }
        }


        // compute positions
        let mut positions: HashMap<HEdgeInput, usize> = HashMap::new();
        let mut inv_positions: HashMap<usize, HEdgeInput> = HashMap::new();
        let mut ckey_min_el: Vec<(HEdgeInput, usize)> = clusters.iter().map(|(e, cluster)| {
            let min_el = min(cluster).unwrap();
            (*e, *min_el)
        }).collect();


        ckey_min_el.sort_by(|a,b| {
            let (_, a_min) = a;
            let (_, b_min) = b;
            a_min.cmp(b_min)
        });

        for (i, (ckey, _)) in ckey_min_el.iter().enumerate() {
            positions.insert(*ckey, i);
            inv_positions.insert(i, *ckey);
        }

        // raw to logic
        for (raw_time, chead_time) in mappings_to_head.iter() {
            let chead = raw_time_to_cluster_key.get(chead_time).unwrap(); // raw time -> head HEdgeInput
            let logic_time = positions.get(chead).unwrap(); // HEdgeInput -> logic time
            raw_to_logic_mappings.insert(*raw_time, *logic_time);
        }


        return Clusters { clusters, mappings_to_head, raw_to_logic_mappings, raw_time_to_cluster_key, positions, inv_positions, clustering_functions: HashMap::new() };
    }

    pub fn get(&self, e: &HEdgeInput) -> Option<&Vec<usize>> {
        self.clusters.get(e)
    }

    pub fn cluster_key(&self, raw_time: usize) -> Option<&HEdgeInput> {
        self.raw_time_to_cluster_key.get(&raw_time)
    }

    pub fn map_raw_to_logic(&self, raw_time: usize) -> usize {
        // *self.raw_to_logic_mappings.get(&raw_time).unwrap()
        match self.raw_to_logic_mappings.get(&raw_time) {
            Some(t) => *t,
            None => {panic!("Cannot convert {} raw time to logic time.", raw_time);}
        }
    }

    pub fn logic_traces_len(&self) -> usize {
        self.positions.len()
    }

    pub fn keep_vector(&self) -> Vec<bool> { // in the perfect case, we project to cluster head
        let mut keep_vector: Vec<bool> = Vec::new();

        for (_, dest) in self.mappings_to_head.iter() {
            let dest = *dest;

            while dest <= keep_vector.len() {
                keep_vector.push(false);
            }

            keep_vector[dest] = true;
        }

        keep_vector
    }

    pub fn apply_clustering(&mut self, lines: Vec<Vec<u8>>) -> Vec<Vec<u8>> {
        let mut clustered: Vec<Vec<u8>> = Vec::with_capacity(lines.len());
        let logic_len = self.positions.len();

        for line in lines.into_iter() {
            let mut logic_line: Vec<u8> = Vec::new();
            for t_logic in 0..logic_len {
                let chead = self.inv_positions.get(&t_logic).unwrap(); // panic if not found
                let mut working_cluster_value: Vec<u8> = Vec::new();
                for tc in self.clusters.get(&chead).unwrap() {
                    working_cluster_value.push(line[*tc]);
                }
                //we have the vector
                
                // have we already encountered it ?
                let v = match self.clustering_functions.get_mut(chead) {
                    Some(func) => {
                        match func.get(&working_cluster_value) {
                            Some(v) => {// value already encountered
                                *v
                            },
                            None => {//create new value
                                let v = func.len();
                                func.insert(working_cluster_value, v as u8);
                                assert!(v < 256, "Clustering incorrect, more than 256 values.");
                                v as u8
                            }
                        }
                    },
                    None => {
                        let mut func: HashMap<Vec<u8>, u8> = HashMap::new();
                        //create new value 0
                        func.insert(working_cluster_value, 0);
                        self.clustering_functions.insert(*chead, func);
                        0
                    }
                };
                logic_line.push(v);
            }
            clustered.push(logic_line);
        }


        clustered
    }
}



pub fn perfect_raw_to_logic_traces<P1: AsRef<Path>, P2: AsRef<Path>>(raw_path: P1, inputs_path: P1, outputs_path: P1, logic_path: P2, filtering: &SimpleFilteringResults, clusters: &mut Clusters) -> anyhow::Result<()> {
    let batch_size: usize = 10000;
    let mut raw_traces = TraceMatrixLinesJointBatchReader::new(raw_path, inputs_path, outputs_path, batch_size)?;
    let mut logic_traces = TraceMatrixLinesBatchWriter::new(logic_path, clusters.logic_traces_len(), raw_traces.traces_count)?;
    // let cluster_keep_vector = clusters.keep_vector();

    let bar = ProgressBar::new((raw_traces.traces_count / batch_size) as u64);

    while !raw_traces.is_terminated() {
        let batch: Vec<Vec<u8>> = raw_traces.read_batch()?;
        let filtered_batch = filtering.apply_filtering(batch);
        let clustered_batch = clusters.apply_clustering(filtered_batch);
        logic_traces.write_batch(&clustered_batch)?;
        bar.inc(1);
    }
    bar.finish();

    Ok(())
}