use hashbrown::HashMap;
// use std::collections::HashMap; // faster entropy, but slower distrib generation

use itertools::izip;
use rand::{Rng, distributions::Uniform};

pub trait Distribution {
    fn uniform() -> Self;
    const DISTRIBUTION_SIZE: usize;

    fn shannon_entropy(&self) -> f32;
}

// This is a distribution : sum must be 1.
#[derive(Clone,Debug)]
pub struct Distribution2_8 {
    distribution: [f32; Distribution2_8::DISTRIBUTION_SIZE],
}

impl Distribution2_8  {
    // Compute a distribution from a list of value
    pub fn from_vector(vec: &[u8]) -> Distribution2_8 {
        let mut counters: [u64; 256] = [0; 256];
        let mut distrib: [f32; 256] = [0.0f32; 256];
        let tot = vec.len() as f32;

        for v in vec {
            counters[*v as usize] += 1;
        }

        for i in 0..256 {
            distrib[i] = (counters[i] as f32) / tot;
        }

        return Distribution2_8 { distribution: distrib }
    }

    pub fn samples_uniform(sample_count: u32) -> Vec<u8> {
        let mut rng = rand::thread_rng();
        let dist = Uniform::new(0, 255);

        let mut samples = Vec::new();

        for _ in 0..sample_count {
            samples.push(rng.sample(dist) as u8);
        }        

        return samples;
    }

    pub fn samples(&self, number_of_samples: usize) -> Vec<u8> {
        let mut rng = rand::thread_rng();
        let mut samples: Vec<u8> = Vec::with_capacity(number_of_samples);

        let mut scale: Vec<f32> = Vec::with_capacity(256);
        let mut start_high: usize = 256;

        let mut counter = 0f32;

        scale.push(0.0f32);
        for val in 0..256 {
            counter += self.distribution[val];
            scale.push(counter);

            if counter >= 1.0f32 && val < start_high {
                start_high = val + 1;
            }
        }

        for _ in 0..number_of_samples {
            let r: f32 = rng.gen();

            // binary search
            let mut lower_index: usize = 0;
            let mut higher_index: usize = start_high;

            while (higher_index - lower_index) > 1 {
                let mid_index = lower_index + (higher_index - lower_index) / 2;
                if scale[mid_index] <= r {
                    lower_index = mid_index;
                }
                else {
                    higher_index = mid_index;
                }
            }

            samples.push(lower_index as u8);
        }

        return samples;
    }

    /// Generate a distribution of the form [p, p, ..., p, 0, ..., 0]
    pub fn uniform_ending_with_zeros(number_of_zeros: usize) -> Distribution2_8 {
        assert!(number_of_zeros < 256);
        let mut distribution = [0f32; Distribution2_8::DISTRIBUTION_SIZE];
        let p: f32 = 1f32 / (256-number_of_zeros) as f32;
        for i in 0..(256 -number_of_zeros) {
            distribution[i] = p;
        }
        return Distribution2_8 { distribution };
    }
}

impl Distribution for Distribution2_8 {

    fn uniform() -> Distribution2_8 {
        let p = 1.0f32 / 256f32;
        return Distribution2_8 { distribution: [p; Distribution2_8::DISTRIBUTION_SIZE] };
    }

    fn shannon_entropy(&self) -> f32 {
        let mut se = 0.0f32;
    
        for p in self.distribution.iter() {
            if *p > 0.0f32 {
                se += (*p) * (*p).log2();
            }
        }
    
        return -se;
        
    }

    const DISTRIBUTION_SIZE: usize = 256;
}



// pub fn shannon_entropy(vec: &[u8]) -> f32 {
//     let distrib = Distribution2_8::from_vector(vec);
//     return distrib.shannon_entropy();
// }

/// A distributions over 2^16 symbols
#[derive(Clone,Debug)]
pub struct Distribution2_16 {
    distribution: Vec<f32>,
}

impl Distribution2_16 {
    pub fn from_2_vectors(vec1: &[u8], vec2: &[u8]) -> anyhow::Result<Distribution2_16> {
        let mut counters: Vec<u64> = vec![0; Distribution2_16::DISTRIBUTION_SIZE];
        let mut distrib: Vec<f32> = vec![0.0f32; Distribution2_16::DISTRIBUTION_SIZE];
        let tot = vec1.len() as f32;

        if vec2.len() != vec1.len() {
            return Err(anyhow::format_err!("Failing to create distrubution (2^16) from two vectors of different sizes."));
        }

        for (v1, v2) in vec1.iter().zip(vec2) {
            let v: u16 = (*v1 as u16) | ((*v2 as u16) << 8);
            counters[v as usize] += 1;
        }

        for i in 0..(Distribution2_16::DISTRIBUTION_SIZE-1) {
            distrib[i] = (counters[i] as f32) / tot;
        }

        return Ok(Distribution2_16 { distribution: distrib });
    }

}

impl Distribution for Distribution2_16 {

    fn uniform() -> Distribution2_16 {
        let p = 1.0f32 / 65536f32;
        return Distribution2_16 { distribution: vec![p; Distribution2_16::DISTRIBUTION_SIZE] };
    }

    const DISTRIBUTION_SIZE: usize = 256*256;

    fn shannon_entropy(&self) -> f32 {
        let mut se = 0.0f32;
    
        for p in self.distribution.iter() {
            if *p > 0.0f32 {
                se += (*p) * (*p).log2();
            }
        }
    
        return -se;
        
    }
}

#[derive(Clone,Debug)]
pub struct Distribution2_24Dense {
    distribution: Vec<f64>,
}

#[derive(Clone,Debug)]
pub struct Distribution2_24Sparse {
    pub distribution: HashMap<u32, f32>,
}

impl Distribution2_24Dense {

    pub fn from_3_vectors(vec1: &[u8], vec2: &[u8], vec3: &[u8]) -> anyhow::Result<Distribution2_24Dense> {
        let mut counters: Vec<u64> = vec![0; Distribution2_24Dense::DISTRIBUTION_SIZE];
        let mut distrib: Vec<f64> = vec![0.0f64; Distribution2_24Dense::DISTRIBUTION_SIZE];
        let tot = vec1.len() as f64;

        if vec2.len() != vec1.len() || vec3.len() != vec1.len() {
            return Err(anyhow::format_err!("Failing to create distrubution (2^24) from three vectors of different sizes."));
        }

        for (v1, v2, v3) in izip!(vec1, vec2, vec3) {
            let v: u32 = (*v1 as u32) | ((*v2 as u32) << 8) | ((*v3 as u32) << 16);
            counters[v as usize] += 1;
        }

        for i in 0..(Distribution2_24Dense::DISTRIBUTION_SIZE-1) {
            distrib[i] = (counters[i] as f64) / tot;
        }

        return Ok(Distribution2_24Dense { distribution: distrib });
    }
}

impl Distribution for Distribution2_24Dense {

    fn uniform() -> Distribution2_24Dense {
        let p = 1.0f64 / (Distribution2_24Dense::DISTRIBUTION_SIZE as f64);

        return Distribution2_24Dense { distribution: vec![p; Distribution2_24Dense::DISTRIBUTION_SIZE] };
    }

    const DISTRIBUTION_SIZE: usize = 256*256*256;

    fn shannon_entropy(&self) -> f32 {
        let mut se = 0.0f64;
    
        for p in self.distribution.iter() {
            if *p > 0.0f64 {
                se += (*p) * (*p).log2();
            }
        }
    
        return (-se) as f32;
        
    }
}

impl Distribution2_24Sparse {
    pub fn from_3_vectors(vec1: &[u8], vec2: &[u8], vec3: &[u8]) -> anyhow::Result<Distribution2_24Sparse> {
        let tot = vec1.len() as f32;
        let mut counters: HashMap<u32, u32> = HashMap::with_capacity(vec1.len());
        let mut distrib: HashMap<u32, f32> = HashMap::with_capacity(vec1.len());

        if vec2.len() != vec1.len() || vec3.len() != vec1.len() {
            return Err(anyhow::format_err!("Failing to create distrubution (2^24) from three vectors of different sizes."));
        }

        for (v1, v2, v3) in izip!(vec1, vec2, vec3) {
            let v: u32 = (*v1 as u32) | ((*v2 as u32) << 8) | ((*v3 as u32) << 16);
            let lastcount: u32 = *counters.get(&v).unwrap_or(&0);
            counters.insert(v, lastcount+1);
        }

        for (k, count) in counters {
            distrib.insert(k, (count as f32) / tot);
        }

        return Ok(Distribution2_24Sparse { distribution: distrib });
    }

}

impl Distribution for Distribution2_24Sparse {

    fn uniform() -> Distribution2_24Sparse {
        let p = 1.0f32 / (Distribution2_24Sparse::DISTRIBUTION_SIZE as f32);

        let mut distrib: HashMap<u32, f32> = HashMap::with_capacity(Distribution2_24Sparse::DISTRIBUTION_SIZE);
        for i in 0..Distribution2_24Sparse::DISTRIBUTION_SIZE {
            distrib.insert(i as u32, p);
        }
        
        return Distribution2_24Sparse { distribution: distrib };
    }

    const DISTRIBUTION_SIZE: usize = 256*256*256;

    fn shannon_entropy(&self) -> f32 {
        let mut se = 0.0f32;

        for (_, p) in self.distribution.iter() {
            if *p > 0.0f32 {
                se += (*p) * (*p).log2();
            }
        }
    
        return -se;
    }
}


#[cfg(test)]
mod tests {
    use super::{Distribution2_8, Distribution, Distribution2_16, Distribution2_24Dense, Distribution2_24Sparse};

    #[test]
    fn test_distrib_2_8() {
        let vec = Distribution2_8::samples_uniform(100000);
        let distrib = Distribution2_8::from_vector(&vec);
        let se = distrib.shannon_entropy();
        assert!((8f32 - se) < 0.01, "Shannon entropy is {}", se); 
    }


    #[test]
    fn test_distrib_2_16() {
        let vec1 = Distribution2_8::samples_uniform(100000);
        let vec2 = Distribution2_8::samples_uniform(100000);
        let distrib = Distribution2_16::from_2_vectors(&vec1, &vec2).unwrap();
        let se = distrib.shannon_entropy();
        assert!((16f32 - se) < 0.6, "Shannon entropy is {}", se); 

        let uniform = Distribution2_16::uniform();
        let se2 = uniform.shannon_entropy();
        assert!((se2 - 16f32).abs() < 0.001f32, "Shannon entropy of uniform distribution is {}", se2); 
    }

    #[test]
    fn test_distrib_2_24() {
        let vec1 = Distribution2_8::samples_uniform(100000);
        let vec2 = Distribution2_8::samples_uniform(100000);
        let vec3 = Distribution2_8::samples_uniform(100000);
        let distrib1 = Distribution2_24Dense::from_3_vectors(&vec1, &vec2, &vec3).unwrap();
        let distrib2 = Distribution2_24Sparse::from_3_vectors(&vec1, &vec2, &vec3).unwrap();
        let se1 = distrib1.shannon_entropy();
        assert!((24f32 - se1) < 8f32, "Shannon entropy is {}", se1); 

        let se2 = distrib2.shannon_entropy();
        assert!((24f32 - se2) < 8f32, "Shannon entropy is {}", se2); 

        assert!((se2 - se1).abs() < 0.02f32, "Dense and sparse entropies are different: {} (dense) vs {} (sparse)", se1, se2);
 
        let uniform = Distribution2_24Dense::uniform();
        let seu = uniform.shannon_entropy();
        assert!((seu - 24f32).abs() < 0.001f32, "Shannon entropy of uniform distribution is {}", seu); 
    }

    #[test]
    fn test_samples_uniform_with_zeros() {
        let distrib1 = Distribution2_8::uniform_ending_with_zeros(252);
        let se1 = distrib1.shannon_entropy();
        assert!((se1 - 2f32).abs() < 0.01f32, "Shannon entropy is {}", se1);

        let vec1 = distrib1.samples(10000);
        // println!("{:?}", vec1);
        let distrib2 = Distribution2_8::from_vector(&vec1);
        let se2 = distrib2.shannon_entropy();
        assert!((se2 - 2f32).abs() < 0.01f32,  "Shannon entropy is {}", se2);
    }
}