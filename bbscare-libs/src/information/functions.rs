use std::collections::HashMap;

use indicatif::ProgressBar;
use serde::{Serialize, Deserialize};

use crate::loaders::{trace_matrix::TraceMatrixLinesBatchReader, file_storable::FileStorable};

use super::hgraph::{HEdge, HGraph, HEdgeInput};


#[derive(Debug,Clone, Serialize, Deserialize)]
pub struct Functions {
    pub funcs: HashMap<HEdge, HashMap<(u8, u8), u8>>, // (edge -> (2to1function))
    pub logic_trace_len: usize,
}

impl Functions {
    pub fn learn(logic_hg: &HGraph, logic_traces: &mut TraceMatrixLinesBatchReader) -> anyhow::Result<Functions> {
        let mut funcs: HashMap<HEdge, HashMap<(u8, u8), u8>> = HashMap::new();

        // prefill funcs with edges, empty functions for now
        for (edge, _) in logic_hg.edges.iter() {
            funcs.insert(edge.clone(), HashMap::new());
        }

        let steps_count = logic_traces.steps();
        let logic_trace_len = logic_traces.traces_len;
        let bar = ProgressBar::new(steps_count as u64);
        while !logic_traces.is_terminated() {
            let lines = logic_traces.read_batch()?;

            for line in lines {
                for (edge, _) in logic_hg.edges.iter() {
                    //get current function
                    let f = funcs.get_mut(edge).unwrap(); //prefill allow unwrap
                    match edge.inputs {
                        HEdgeInput::Two(i1, i2) => {
                            match f.get(&(line[i1],line[i2])) {
                                Some(v3) => {//check
                                    if *v3 != line[edge.output] {
                                        return Err(anyhow::format_err!("Error in the learning phase for edge {} for input ({},{}).", edge, line[i1], line[i2]));
                                    }
                                },
                                None => {// create entry
                                    f.insert((line[i1], line[i2]), line[edge.output]);
                                }
                            }
                        },
                        HEdgeInput::One(_) => unreachable!() //only 2to1 in logic_hg
                    }
                }
            }
            bar.inc(1);
        }
        bar.finish();


        Ok(Functions { funcs, logic_trace_len })
    }

    pub fn apply(&self, edge: &HEdge, v1: u8, v2: u8) -> Option<u8> {
        self.funcs.get(edge).and_then(|f|f.get(&(v1, v2))).copied()
    }
}

impl FileStorable for Functions {}