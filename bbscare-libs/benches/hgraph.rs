use rand::Rng;

use bbscare_libs::{information::{hgraph::{HGraph, HEdge}, entropies::EntropyEstimator, flow_analysis::{TwoSourcesAnalyzer}}, loaders::{trace_matrix::TraceMatrixColumns, file_storable::FileStorable}};
use criterion::{black_box, criterion_group, criterion_main, Criterion};

pub fn hgraph_add_edge(c: &mut Criterion) {
    let mut hgraph: HGraph = HGraph::new();
    let mut rng = rand::thread_rng();
    // c.bench_function("entropy 2^8", |b| b.iter(|| black_box(distrib.clone()).shannon_entropy()));

    c.bench_function("add_edge", |b| b.iter(|| {
        let i1: usize = rng.gen();
        let i2: usize = rng.gen();
        let i3: usize = rng.gen();
        let edge = HEdge::new2to1(i1, i2, i3);

        hgraph.add_edge(edge, 1f32)
    }));
}

fn fai_from_2_sources(c: &mut Criterion) {

    let traces_columns = TraceMatrixColumns::from_file("../data/poor_traces_if.binu8").unwrap();
    assert_eq!(traces_columns.traces_count(), 50_000);

    let estimator = EntropyEstimator::from_file("../data/entropy_estimators").unwrap();
    let hg = HGraph::from_file("../data/hg").unwrap();
    let analysis = TwoSourcesAnalyzer::new(&traces_columns, 1, 2, &hg, &estimator, 0.1, 256);
    println!("{:?}", analysis.identified_2to1);

    c.bench_function("fai_from_2_sources", |b|b.iter(|| {
        // fai_from_2_sources_under_threshold(&traces_columns, 1, 2, &estimator, 0.6, 256)
        let two_analysis = TwoSourcesAnalyzer::new(&traces_columns, 1, 2, &hg, &estimator, 0.1, 256);
        two_analysis
    }));
}

criterion_group!(benches, fai_from_2_sources);
criterion_main!(benches);