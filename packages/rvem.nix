with import <nixpkgs> {};


let
     pkgs = import (builtins.fetchGit {
         # Descriptive name to make the store path easier to identify                
         name = "nixos-22-11";
        url = "https://github.com/nixos/nixpkgs/";
        ref = "refs/heads/nixos-22.11";
        rev = "ab1254087f4cdf4af74b552d7fc95175d9bdbb49";                                           
     }) {};                                                                           
in

rustPlatform.buildRustPackage rec {
    pname = "rvem";
    version = "0.1.0";

    src = fetchgit {
        url = "https://gitlab.inria.fr/bbscare/rvem.git";
        hash = "sha256-fwFsCUxgv1jqUhR7kGpuNDj8UEmjmMtU7PY+drOHz88=";
    };

    cargoSha256 = "1yrh8f9xgparhpkkyacdad750vbjr6chl1wnah7k8f326xg3q8gh";
    buildAndTestSubdir = "rvem";
    # cargoLock = {
    #     lockFile = ./rvem/Cargo.lock;
    # };

    meta = with lib; {
        description = "A RISCV emulator that generate traces";
        homepage = "https://gitlab.inria.fr/bbscare/rvem";
        license = licenses.unlicense;
        maintainers = [ maintainers.tailhook ];
    };
}