use bbscare_libs::information::entropies::{EntropyEstimator};

fn main() {
    //Searching for entropy estimator file
    let mut estimator = {
        match EntropyEstimator::from_file("entropy_estimators") {
            Ok(e) => { 
                println!("File entropy_estimators found.");
                println!("Included estimators:");
                for (s, d) in e.list_estimators_parameters() {
                    println!("\t{} samples, {} dimensions.", s, d);
                }
                e
            },
            Err(_) => {
                println!("No file found, creating a new estimator.");
                EntropyEstimator::new()
            }
        }
    };

    println!("Creating 100000, 1");
    estimator.add_estimator(100000,1);
    println!("Creating 100000, 2");
    estimator.add_estimator(100000,2);
    println!("Creating 100000, 3");
    estimator.add_estimator(100000,3);

    match estimator.save_to_file("entropy_estimators") {
        Ok(_) => {
            println!("File saved.");
        },
        Err(e) => {
            println!("Cannot save file : {}", e);
        }
    }
}
